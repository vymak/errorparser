﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ErrorParser
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Proměnná určující povolené typy souborů
        /// </summary>
        private string[] _allowedExtension = { ".txt", ".log" };

        /// <summary>
        /// List udržující rozparserované chyby
        /// </summary>
        private List<ErrorInfo> _errorList;

        /// <summary>
        /// Pole pro převod z listu
        /// </summary>
        private ErrorInfo[] _errorArray;

        /// <summary>
        /// Proměnná udžující aktuální index
        /// </summary>
        public int _index { get; private set; }

        /// <summary>
        /// Proměnná udržující informaci, zda-li se má appendovat na konec souboru
        /// </summary>
        private bool _append { get; set; }


        private event EventHandler OnListCountChanged;

        public Form1()
        {
            InitializeComponent();
            _append = false;
            _errorList = new List<ErrorInfo>();
            OnListCountChanged += Form1_OnListCountChanged;
            toolTip1.SetToolTip(appendCheckbox, "If selected file is current exist, you can append text after existed text. If you creating new file, this option has no effect.");
        }

        private void Form1_OnListCountChanged(object sender, EventArgs e)
        {
            if (_errorList.Count != 0)
            {
                if (_errorList.Count > 1)
                    nextButton.Enabled = true;

                saveButton.Enabled = true;
                appendCheckbox.Enabled = true;
                removeLinkLabel.Enabled = true;
                _index = 0;
                _errorArray = _errorList.ToArray();
                this.ShowErrors();
            }
            else
            {
                appendCheckbox.Enabled = false;
                saveButton.Enabled = false;
                nextButton.Enabled = false;
                prevouisButton.Enabled = false;
                removeLinkLabel.Enabled = false;
                MessageBox.Show("You have no other error in error list!", "No error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadLogButton_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                FileInfo file = new FileInfo(openFileDialog1.FileName);

                if (!_allowedExtension.Contains(file.Extension))
                    MessageBox.Show("You entered a file with not allowed extension. Please select another file!", "Wrong extension", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    string[] arrayError = this.ParseLog(file.FullName);
                    _errorList = this.ParseDataToList(arrayError);
                    OnListCountChanged(sender, EventArgs.Empty);
                }
            }
        }

        private string[] ParseLog(string filename)
        {
            string tmp = String.Empty;
            try
            {
                using (StreamReader str = new StreamReader(filename))
                {
                    tmp = str.ReadToEnd();
                }
            }
            catch (IOException)
            {
                MessageBox.Show("An error occured during parsing a log file. Please try open again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            string strednik = String.Empty;
            for (int i = 0; i < tmp.Length; i++)
            {
                if (tmp[i] == '[')
                {
                    strednik += ";" + tmp[i];
                }
                else
                    strednik += tmp[i];
            }

            string[] res = strednik.Split(';');

            return res;
        }

        private List<ErrorInfo> ParseDataToList(string[] errors)
        {
            List<ErrorInfo> list = new List<ErrorInfo>();
            string pattern = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}-[0-9]{2}-[0-9]{2}";
            foreach (string item in errors)
            {
                Regex reg = new Regex(pattern);
                Match success = reg.Match(item);

                if (!success.Success)
                    continue;

                ErrorInfo tmp = new ErrorInfo();
                tmp.dateError = ErrorInfo.ParseDate(success.Value);
                tmp.error = item.Replace(success.Value, String.Empty).Replace("[", "").Replace("]", "").Trim();
                list.Add(tmp);
            }

            return list;
        }

        private void NavigationButtonClick(object sender, EventArgs e)
        {
            if (sender == nextButton)
            {
                if (_index < _errorArray.Length - 1)
                {
                    _index++;
                    prevouisButton.Enabled = true;
                }
                else
                    nextButton.Enabled = false;
            }
            else
            {
                if (_index > 0)
                {
                    _index--;
                    nextButton.Enabled = true;
                }
                else
                    prevouisButton.Enabled = false;
            }
            this.ShowErrors();
        }

        private void ShowErrors()
        {
            dateErrorLabel.Visible = true;
            richTextBox1.Text = _errorArray[_index].error;
            dateErrorLabel.Text = _errorArray[_index].dateError.ToString();
        }

        private void removeLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            _errorList.RemoveAt(_index);
            OnListCountChanged(sender, EventArgs.Empty);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            DialogResult res = saveFileDialog1.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                using (StreamWriter str = new StreamWriter(saveFileDialog1.FileName, _append))
                {
                    foreach (ErrorInfo item in _errorList)
                    {
                        str.WriteLine(item.dateError);
                        str.WriteLine(item.error);
                        str.WriteLine("------------------------------------------------");
                    }
                }
            }
        }

        private void appendCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (appendCheckbox.Checked)
                _append = true;
            else
                _append = false;
        }
    }
}
