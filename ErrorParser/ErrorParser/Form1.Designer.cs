﻿namespace ErrorParser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.loadLogButton = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.dateErrorLabel = new System.Windows.Forms.Label();
            this.prevouisButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.removeLinkLabel = new System.Windows.Forms.LinkLabel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.saveButton = new System.Windows.Forms.Button();
            this.appendCheckbox = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "TXT file|*.txt|Log file|*.log";
            // 
            // loadLogButton
            // 
            this.loadLogButton.Location = new System.Drawing.Point(605, 12);
            this.loadLogButton.Name = "loadLogButton";
            this.loadLogButton.Size = new System.Drawing.Size(91, 23);
            this.loadLogButton.TabIndex = 0;
            this.loadLogButton.Text = "Load error log";
            this.loadLogButton.UseVisualStyleBackColor = true;
            this.loadLogButton.Click += new System.EventHandler(this.loadLogButton_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 94);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(684, 166);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // dateErrorLabel
            // 
            this.dateErrorLabel.AutoSize = true;
            this.dateErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateErrorLabel.Location = new System.Drawing.Point(12, 67);
            this.dateErrorLabel.Name = "dateErrorLabel";
            this.dateErrorLabel.Size = new System.Drawing.Size(60, 24);
            this.dateErrorLabel.TabIndex = 2;
            this.dateErrorLabel.Text = "label1";
            this.dateErrorLabel.Visible = false;
            // 
            // prevouisButton
            // 
            this.prevouisButton.Enabled = false;
            this.prevouisButton.Location = new System.Drawing.Point(12, 266);
            this.prevouisButton.Name = "prevouisButton";
            this.prevouisButton.Size = new System.Drawing.Size(75, 23);
            this.prevouisButton.TabIndex = 3;
            this.prevouisButton.Text = "<< Prevouis";
            this.prevouisButton.UseVisualStyleBackColor = true;
            this.prevouisButton.Click += new System.EventHandler(this.NavigationButtonClick);
            // 
            // nextButton
            // 
            this.nextButton.Enabled = false;
            this.nextButton.Location = new System.Drawing.Point(621, 266);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 4;
            this.nextButton.Text = "Next >>";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.NavigationButtonClick);
            // 
            // removeLinkLabel
            // 
            this.removeLinkLabel.AutoSize = true;
            this.removeLinkLabel.Enabled = false;
            this.removeLinkLabel.Location = new System.Drawing.Point(326, 271);
            this.removeLinkLabel.Name = "removeLinkLabel";
            this.removeLinkLabel.Size = new System.Drawing.Size(47, 13);
            this.removeLinkLabel.TabIndex = 5;
            this.removeLinkLabel.TabStop = true;
            this.removeLinkLabel.Text = "Remove";
            this.removeLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.removeLinkLabel_LinkClicked);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "TXT file|*.txt|LOG file|*.log";
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(12, 12);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(101, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Save errors to file";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // appendCheckbox
            // 
            this.appendCheckbox.AutoSize = true;
            this.appendCheckbox.Enabled = false;
            this.appendCheckbox.Location = new System.Drawing.Point(12, 41);
            this.appendCheckbox.Name = "appendCheckbox";
            this.appendCheckbox.Size = new System.Drawing.Size(111, 17);
            this.appendCheckbox.TabIndex = 7;
            this.appendCheckbox.Text = "Append text to file";
            this.appendCheckbox.UseVisualStyleBackColor = true;
            this.appendCheckbox.CheckedChanged += new System.EventHandler(this.appendCheckbox_CheckedChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 308);
            this.Controls.Add(this.appendCheckbox);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.removeLinkLabel);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.prevouisButton);
            this.Controls.Add(this.dateErrorLabel);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.loadLogButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Parser for Nette Framework generated log";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button loadLogButton;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label dateErrorLabel;
        private System.Windows.Forms.Button prevouisButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.LinkLabel removeLinkLabel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.CheckBox appendCheckbox;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

