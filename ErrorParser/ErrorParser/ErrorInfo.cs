﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorParser
{
    public class ErrorInfo
    {
        public DateTime dateError { get; set; }
        public string error { get; set; }

        public static DateTime ParseDate(string date)
        {
            DateTime dateTime = new DateTime();
            string format = "yyyy-MM-dd hh-mm-ss";
            bool success = DateTime.TryParseExact(date, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (success)
                return dateTime;
            return DateTime.Now;
        }
    }
}
